﻿using NSDesai.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NSDesai.Controllers
{
    public class HomeController : Controller
    {

        private nconin_PropertyUATEntities dbset;

        public HomeController()
        {
            dbset = new nconin_PropertyUATEntities();
        }

        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(FormCollection frm)
        {
            string username = Convert.ToString(frm["emailid"]);
            string passwd = Convert.ToString(frm["txtpasswd"]);

            var result = dbset.tblstaffs.Where(x => x.staffemail == username && x.password == passwd).FirstOrDefault();

            if (result != null)
            {
                Session["Username"] = result.staffname;
                return Redirect("~/Inquiry/Index");
            }
            else
            {
                return View();
            }

            
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult LoginPage()
        {
            return View();
        }
    }
}