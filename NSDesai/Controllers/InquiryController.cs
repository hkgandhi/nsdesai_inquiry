﻿using NSDesai.ModelClass;
using NSDesai.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Validation;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace NSDesai.Controllers
{
   [ValidateModelStateAttributes]
    public class InquiryController : Controller
    {
        private nconin_PropertyUATEntities dbset;
       
        public InquiryController()
        {
            dbset = new nconin_PropertyUATEntities();
        }
        // GET: Inquiry
        [HttpGet]
        public ActionResult Index(string mobileno = null)
        {
            FormCollection frm = new FormCollection();
            //string mobileno1 = Request.Form["mobileno"];                      
            //dynamic mutiplemodel = new ExpandoObject();
            //mutiplemodel.calldetails = new tblcall();
            //mutiplemodel.callerdetails = new tblcaller();   
            tblcall calldetails = new tblcall();
            tblcaller callerdetails = new tblcaller();
            if (mobileno != null)
            {
                ViewBag.MobielNoEx = mobileno;
                callerdetails.idcaller = dbset.tblcallers.Where(x => x.Mobile == mobileno).Select(x => x.idcaller).FirstOrDefault();
            }
            var TupleModel = new Tuple<tblcall,tblcaller>(calldetails,callerdetails);
            List<string> propertylist = ConfigurationManager.AppSettings["PropertyName"].ToString().Split(',').Select(x => x.Trim()).ToList();
            ViewBag.propertylist = propertylist;
            List<string> callertypelist = ConfigurationManager.AppSettings["Callertype"].ToString().Split(',').Select(x => x.Trim()).ToList();
            ViewBag.callertypelist = callertypelist;
            List<string> propertype = ConfigurationManager.AppSettings["Propertytype"].ToString().Split(',').Select(x => x.Trim()).ToList();
            ViewBag.propertypelist = propertype;            
            return View(TupleModel);
        }

        [HttpPost]
      //  [ValidateModelStateAttributes]
      //  [ValidateModelStateAttributes]
        public ActionResult Index(tblcall item1,tblcaller item2)
        {
            item1.idstaff = 0;
            if (ModelState.IsValid)
            {
                try
                {

                    if (item2.idcaller > 0)
                    {
                        item1.idcaller = item2.idcaller;
                        dbset.tblcalls.Add(item1);
                    }
                    else
                    {
                        dbset.tblcallers.Add(item2);
                        dbset.SaveChanges();
                        item1.idcaller = item2.idcaller;
                        dbset.tblcalls.Add(item1);
                    }


                    dbset.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    Exception raise = ex;
                    foreach (var validationErrors in ex.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting  
                            // the current instance as InnerException  
                            raise = new InvalidOperationException(message, raise);
                        }
                    }

                    throw raise;
                }

                return RedirectToAction("GetGridList");
            }
            else
            {
                item2.idcaller = 0;
                List<string> propertylist = ConfigurationManager.AppSettings["PropertyName"].ToString().Split(',').Select(x => x.Trim()).ToList();
                ViewBag.propertylist = propertylist;
                List<string> callertypelist = ConfigurationManager.AppSettings["Callertype"].ToString().Split(',').Select(x => x.Trim()).ToList();
                ViewBag.callertypelist = callertypelist;
                List<string> propertype = ConfigurationManager.AppSettings["Propertytype"].ToString().Split(',').Select(x => x.Trim()).ToList();
                ViewBag.propertypelist = propertype;
                var TupleModel = new Tuple<tblcall, tblcaller>(new tblcall(), new tblcaller());
                return View(TupleModel);
            }
        }
        
        [HttpGet]
        public JsonResult GetCallerInquirtyByMobile(string Mobile)
        {
            var CallerData = dbset.tblcallers.Where(x => x.Mobile == Mobile).FirstOrDefault();
            var InquiryData = dbset.tblcalls.Where(x => x.idcaller == CallerData.idcaller).ToArray();
            CallerWithInquiryModel CImodel = new CallerWithInquiryModel();
            CImodel.callerDetails = CallerData;
            CImodel.inquiryDetails = InquiryData;
            return Json(CImodel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetGridList()
        {
            return View();
        }
        [HttpGet]
        public JsonResult GetAllCallerDetails()
        {
            var inquirywithcaller = (from i in dbset.tblcalls
                                     join j in dbset.tblcallers on i.idcaller equals j.idcaller
                                     select new { txtrequirementtype = i.txtrequirementtype, txtinquiryproject = i.txtinquiryproject, txtpurpose = i.txtpurpose, FirstName = j.FirstName, Mobile = j.Mobile, City = j.City, Email = j.Email,callerid = j.idcaller }).ToList();
                                   
            return Json(inquirywithcaller,JsonRequestBehavior.AllowGet);
        }        
    }
}