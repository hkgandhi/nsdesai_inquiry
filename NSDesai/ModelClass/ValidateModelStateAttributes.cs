﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NSDesai.ModelClass
{
    public class ValidateModelStateAttributes : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {


            HttpContext ctx = HttpContext.Current;
            if (HttpContext.Current.Session["Username"] == null)
            {
                filterContext.Result = new RedirectResult("~/Home/Index");
                return;
            }

            //if (!viewdata.ModelState.IsValid)
            //{
            //    filterContext.Result = new ViewResult()
            //    {
            //        ViewData = viewdata,
            //        TempData = filterContext.Controller.TempData
            //    };
            //}
            base.OnActionExecuting(filterContext);
        }

        
    }
}